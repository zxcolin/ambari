![输入图片说明](https://images.gitee.com/uploads/images/2021/0112/180041_b10a3613_538014.png "Screenshot from 2021-01-12 18-00-20.png")

## 介绍
Ambari是Apache开源的Hadoop平台的管理软件，具备Hadoop组件的安装、管理、运维等基本功能，提供Web UI进行可视化的集群管理，简化了大数据平台的安装、使用难度。
本项目基于Ambari-2.7.4进行了二次开发，主要包括汉化Ambari,集成新的组件，如Grafana,Prometheus,MySQL,Clickhouse,Azkaban,Greenplum等等。


### 编译安装

1.  编译

mvn -B clean install package rpm:rpm -DnewVersion=2.7.4.0.版本号 -DskipTests -Drat.skip -Dpython.ver="python >= 2.6"

![编译源码](https://images.gitee.com/uploads/images/2021/0112/174915_c79431b3_538014.png "compile.png")

2.  手动或使用一键部署脚本安装

手动安装参考Apache官网:
https://cwiki.apache.org/confluence/display/AMBARI/Quick+Start+Guide

一键部署脚本参考如下:

![一键部署脚本](https://images.gitee.com/uploads/images/2021/0112/174809_ca2a6450_538014.png "Screenshot from 2021-01-12 17-47-52.png")

3. 安装效果

![安装效果](https://images.gitee.com/uploads/images/2021/0113/084404_be4ba7e0_538014.png "33.png")

### 联系
    邮箱：zhangxiongcolin@126.com

    微信公众号:

![公众号](https://images.gitee.com/uploads/images/2021/0112/175041_bc3abb70_538014.png "beardata.png")