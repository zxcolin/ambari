#!/usr/bin/env python
# -*- coding: utf-8 -*--

import os
import sys
from ambari_commons.os_family_impl import OsFamilyFuncImpl, OsFamilyImpl
from resource_management import *

reload(sys)
sys.setdefaultencoding('utf-8')


@OsFamilyFuncImpl(os_family=OsFamilyImpl.DEFAULT)
def grafana(name=None):
    import params

    Directory(params.grafana_log_dir,
              owner=params.grafana_user,
              create_parents=True,
              group=params.user_group,
              mode=0755
              )

    Directory(params.grafana_pid_dir,
              owner=params.grafana_user,
              create_parents=True,
              group=params.user_group,
              mode=0755
              )

    create_conf_link = format("ln -sb {stack_root}/current/{component_directory}/conf /etc/grafana")
    Execute(create_conf_link, user=params.grafana_user)

    grafana_conf = InlineTemplate(params.grafana_conf)
    File(os.path.join(params.grafana_config_dir, 'defaults.ini'),
         owner=params.grafana_user,
         group=params.user_group,
         content=grafana_conf
         )
