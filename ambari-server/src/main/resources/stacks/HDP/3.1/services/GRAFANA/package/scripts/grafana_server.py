from grafana import grafana
from grafana_service import grafana_service
from resource_management import *
from resource_management import *
from resource_management.libraries.functions import check_process_status


class GrafanaMaster(Script):
    def install(self, env):
        import params
        env.set_params(params)
        # configure the correct package name(metainfo.xml) so that stack-select can find it
        self.install_packages(env)

    def configure(self, env):
        import params
        env.set_params(params)
        grafana(name='grafana')
        print
        'configure grafana server done!'

    def start(self, env):
        import params
        env.set_params(params)
        self.configure(env)  # for security reason
        grafana_service('grafana', action='start')

    def stop(self, env):
        import params
        env.set_params(params)
        grafana_service('grafana', action='stop')

    def restart(self, env):
        self.stop(env)
        self.start(env)

    def status(self, env):
        import params
        check_process_status(params.grafana_pid_file)


if __name__ == "__main__":
    GrafanaMaster().execute()
