from resource_management import *


def grafana_service(
        name,
        action='start_master'):  # 'start' or 'stop' or 'status'
    # initializing the op command variables
    import params
    grafana_pid_file = params.grafana_pid_file
    grafana_pid_expression = as_sudo(["cat", grafana_pid_file])
    grafana_no_op_test = as_sudo(["test", "-f", grafana_pid_file]) + format(
        " && ps -p `{grafana_pid_expression}` >/dev/null 2>&1")

    if action == 'start':
        print
        'start grafana server'
        cmd = format("cd {grafana_bin}; nohup ./grafana-server web > {grafana_log_dir}/grafana.log 2>&1 &")
        Execute(cmd,
                not_if=grafana_no_op_test,
                user=params.grafana_user
                )
        grafana_pid_cmd = format("pgrep -of grafana-server > {grafana_pid_file}")
        Execute(grafana_pid_cmd, user=params.grafana_user)
    elif action == 'stop':
        print
        'stop grafana server'
        # cmd = format("kill -9 " + str(int(sudo.read_file(grafana_pid_file))))
        cmd = format("pkill -9 grafana-server")
        Execute(cmd,
                user=params.grafana_user,
                only_if="netstat -tupln | egrep ':{0}' ".format(params.grafana_port)
                )
        File(grafana_pid_file, action="delete")
    else:
        print
        'unknown command type!'
