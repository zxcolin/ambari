from resource_management import *
from resource_management.libraries.script.script import Script
from resource_management.libraries.functions.default import default
import os
import status_params

# server configurations
config = Script.get_config()
Logger.info("Default system config:"+ str(config))

service_packagedir = os.path.realpath(__file__).split('/scripts')[0]
tmp_dir = Script.get_tmp_dir()

# Stack info
stack_version_unformatted = str(config['clusterLevelParams']['stack_version'])
stack_version_formatted = format_stack_version(stack_version_unformatted)
stack_root = Script.get_stack_root()
stack_name = default("/hostLevelParams/stack_name", None)
component_directory = status_params.component_directory
user_group = config['configurations']['cluster-env']['user_group']
grafana_home = format("{stack_root}/current/{component_directory}")
grafana_bin = format("{stack_root}/current/{component_directory}/bin")

ambari_server_hostname = config['ambariLevelParams']['ambari_server_host']

current_host_name = config['agentLevelParams']['hostname']

# Grafana Configurations
grafana_user = config['configurations']['grafana']['grafana_user']
grafana_conf = config['configurations']['grafana']['grafana_conf']
grafana_port = config['configurations']['grafana']['grafana_port']
grafana_log_dir = config['configurations']['grafana']['grafana_log_dir']
grafana_pid_dir = config['configurations']['grafana']['grafana_pid_dir']
grafana_pid_file = format("{grafana_pid_dir}/grafana.pid")
grafana_config_dir = config['configurations']['grafana']['grafana_config_dir']