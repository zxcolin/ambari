#!/usr/bin/env python
"""
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

"""
from resource_management import *
from resource_management.libraries.script.script import Script
from resource_management.libraries.functions.default import default
import os
import status_params

# server configurations
config = Script.get_config()
Logger.info("Default system config:" + str(config))

service_packagedir = os.path.realpath(__file__).split('/scripts')[0]
tmp_dir = Script.get_tmp_dir()

# Stack info
stack_version_unformatted = str(config['clusterLevelParams']['stack_version'])
stack_version_formatted = format_stack_version(stack_version_unformatted)
stack_root = Script.get_stack_root()
stack_name = default("/hostLevelParams/stack_name", None)
component_directory = status_params.component_directory
user_group = config['configurations']['cluster-env']['user_group']
prometheus_home = format("{stack_root}/current/{component_directory}")

ambari_server_hostname = config['ambariLevelParams']['ambari_server_host']

current_host_name = config['agentLevelParams']['hostname']

# prometheus Configurations
prometheus_user = config['configurations']['prometheus']['prometheus_user']
prometheus_conf = config['configurations']['prometheus']['prometheus_conf']
prometheus_port = config['configurations']['prometheus']['prometheus_port']
prometheus_log_dir = config['configurations']['prometheus']['prometheus_log_dir']
prometheus_pid_dir = config['configurations']['prometheus']['prometheus_pid_dir']
prometheus_pid_file = format("{prometheus_pid_dir}/prometheus.pid")
