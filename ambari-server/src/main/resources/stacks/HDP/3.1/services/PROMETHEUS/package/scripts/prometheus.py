#!/usr/bin/env python
"""
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

"""
import os
from ambari_commons.os_family_impl import OsFamilyFuncImpl, OsFamilyImpl
from resource_management import *


@OsFamilyFuncImpl(os_family=OsFamilyImpl.DEFAULT)
def prometheus(name=None):
    import params

    Directory(params.prometheus_log_dir,
              owner=params.prometheus_user,
              create_parents=True,
              group=params.user_group,
              mode=0755
              )

    Directory(params.prometheus_pid_dir,
              owner=params.prometheus_user,
              create_parents=True,
              group=params.user_group,
              mode=0755
              )

    prometheus_configurations = params.config['configurations']['prometheus']
    File(os.path.join(params.prometheus_home, 'prometheus.yml'),
         content=Template("prometheus.yml.j2", configurations=prometheus_configurations),
         owner=params.prometheus_user,
         group=params.user_group
         )
