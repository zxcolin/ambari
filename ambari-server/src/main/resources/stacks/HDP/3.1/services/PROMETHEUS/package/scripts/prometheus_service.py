#!/usr/bin/env python
"""
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

"""
from resource_management import *


def prometheus_service(
        name,
        action='start_master'):  # 'start' or 'stop' or 'status'
    # initializing the op command variables
    import params
    prometheus_pid_file = params.prometheus_pid_file
    prometheus_pid_expression = as_sudo(["cat", prometheus_pid_file])
    prometheus_no_op_test = as_sudo(["test", "-f", prometheus_pid_file]) + format(
        " && ps -p `{prometheus_pid_expression}` >/dev/null 2>&1")

    if action == 'start':
        cmd = format(
            "cd {prometheus_home} ;nohup ./prometheus > {prometheus_log_dir}/prometheus.log 2>&1 & echo $! > {prometheus_pid_file}")
        Execute(cmd,
                not_if=prometheus_no_op_test,
                user=params.prometheus_user
                )
    elif action == 'stop':
        cmd = format("kill -TERM $(pgrep -f prometheus)")
        Execute(cmd,
                user=params.prometheus_user,
                only_if="netstat -tupln | egrep ':{0}' ".format(params.prometheus_port)
                )
        File(prometheus_pid_file, action="delete")
    else:
        Logger.ingo('unknown command type!')
